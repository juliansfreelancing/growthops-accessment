const { defineConfig } = require('@vue/cli-service')
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = defineConfig({
  chainWebpack: config => {
    config.resolve.alias
      .set('@components', resolve('src/components'))
      .set('@directives', resolve('src/directives'))
      .set('@utils', resolve('src/utils'))
      .set('@api', resolve('src/api'))
  },
  devServer: {
    open: {
      target: ['http://localhost:8080'],
    },
    proxy: {
      '^/api': {
        target: 'http://localhost:8085',
        changeOrigin: true,
        ws: true,
        secure: false,
        pathRewrite: { "^/api": "/" }
      },
    },
  },
})
