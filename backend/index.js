const BookList = require('./Booklist.json')
const express = require('express')
const app = express()

// get all words from book list
let words = []
BookList.forEach(book => {
  for (const key of Object.keys(book)) {
    // if (typeof book[key] !== 'string') continue
    const arr = book[key].toString().toLowerCase().split(' ')
    words = [ ...words, ...arr ]
  }
})

// return all words from book list
app.get('/words', function (req, res) {
  res.send(words)
})

// allows retrieval of books
app.get('/books', function(req, res) {
  let page = req.query.page
  let perPage = req.query.perPage
  let keyword = req.query.keyword

  let buf = ".*" + keyword.replace(/(.)/g, "$1.*").toLowerCase();
  let reg = new RegExp(buf);
  let filteredBooks = keyword !== '' ? BookList.filter(function (e) {
    return reg.test(e.author.toString().toLowerCase()) || reg.test(e.series.toString().toLowerCase()) || reg.test(e.name.toString().toLowerCase())
  }) : BookList

  // split into pages
  let pageChunk = []
  for (let i = 0; i < filteredBooks.length; i += perPage) {
    pageChunk.push(filteredBooks.slice(i, i + perPage))
  }

  res.send(JSON.stringify({
    page: page,
    perPage: perPage,
    total: filteredBooks.length,
    data: pageChunk[page - 1],
  }))
})

app.listen(8085, () => {
  console.info('App listening to port 8085')
})
