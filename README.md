To launch the project, use the command below:

```
npm run serve:both
```

The fontend is built using Vue.js. The autocompletion on frontend, relies on backend to return a list of words. This is done through api call when the app launches.

The backend is built using node js. It generates a list of words by looping through all the words in the booklist.json. Ideally, these information should be stored in the database but i've used the json file as a replacement.

