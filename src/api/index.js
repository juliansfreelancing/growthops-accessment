import axios from '@utils/axios'
import { toQueryString } from '@utils/index'

export function getWords() {
  return axios.get('/words')
}

export function getBooks(params = {}) {
  return axios.get(`/books` + toQueryString(params))
}
