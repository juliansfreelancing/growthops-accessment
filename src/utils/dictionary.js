/***
 * Uses js object as an tree. To search for suggestions.
 */
export default class Dictionary {
  constructor() {
    this.tree = null
    this.suggestions = []
  }

  newNode() {
    return {
      isChild: false,
      children: {},
    }
  }

  addWord(word = '') {
    if (!this.tree) this.tree = this.newNode()

    let root = this.tree
    // loop and update each characters into the tree
    for (const letter of word) {
      if (!(letter in root.children)) {
        root.children[letter] = this.newNode()
      }
      root = root.children[letter]
    }
    root.isChild = true
  }

  /***
   * Find and return possible word combination
   * @param word
   * @returns root - object containing the possible word combinations
   */
  findPossibleCombination(word = '') {
    let root = this.tree
    for (const letter of word) {
      if (letter in root.children) {
        root = root.children[letter]
      } else {
        return null
      }
    }
    return root
  }

  traverseCombination(root, word) {
    if (root.isChild) {
      this.suggestions.push(word)
      return
    }

    for (const letter in root.children) {
      this.traverseCombination(root.children[letter], word + letter)
    }
  }

  getSuggestions(word = '') {
    this.suggestions = []
    // check for empty dictionary
    if (Object.entries(this.tree).length < 1 || word === '') return this.suggestions

    const combination = this.findPossibleCombination(word)
    if (!combination) return this.suggestions

    const children = combination.children

    for (const letter in children) {
      this.traverseCombination(children[letter], word + letter)
    }

    return this.suggestions
  }
}
