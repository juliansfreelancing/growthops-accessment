import axios from 'axios'

let http = {}
const axiosInstance = axios.create({
  baseURL: '/api',
})

axiosInstance.interceptors.response.use(function(res) {
  return res.data
})

http.get = function(url = '') {
  return new Promise((resolve, reject) => {
    axiosInstance.get(url).then(res => {
      resolve(res)
    }).catch(err => {
      console.error(err)
      reject()
    })
  })
}

export default http
